<?php
/**
* 
*/
class Database
{

	function execute($query) //fungsi execute g digunakan untuk menjalankan query dan mengembalikan status dari query tersebut
	{
		include("database_connect.php");
		if (mysqli_query($conn,$query)) {
			# code...
			mysqli_close($conn);
			return true;

		}

		mysqli_close($conn);

		return false;
	}
	/*
	function __construct(argument)
	{
		# code...
	}
	*/

	function get($query) //fungsi get untuk menjalankan query dan mengembalikan data 
	{
		include ("database_connect.php");
		$result = $conn->query($query);

		if ($result) 
		{
		 	$conn->close();
			return $result;
			# code...
		}

		$conn->close();
		return null;

	}

	function get_procedure_execute($procedure) //fungsi yg digunakan untuk menjalankan prosedur dan mengembalikan status query
	{
		include ("database_connect.php");
		return mysqli_query($conn,"CALL ".$procedure);
	}
}

?>