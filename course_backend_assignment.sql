-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 07, 2019 at 03:23 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `course_backend_assignment`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_tbl`
--

CREATE TABLE `account_tbl` (
  `account_id` int(255) NOT NULL,
  `uid` int(255) NOT NULL,
  `game_id` int(255) NOT NULL,
  `account_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account_tbl`
--

INSERT INTO `account_tbl` (`account_id`, `uid`, `game_id`, `account_name`) VALUES
(2, 4, 1, 'asd'),
(3, 4, 2, 'asasdad'),
(4, 5, 2, 'dfgdfgasd'),
(5, 6, 3, 'asbfafad'),
(6, 7, 1, 'as12312d'),
(7, 9, 2, 'asdaasd'),
(8, 10, 1, 'asdasdasd');

-- --------------------------------------------------------

--
-- Table structure for table `game_tbl`
--

CREATE TABLE `game_tbl` (
  `game_id` int(255) NOT NULL,
  `game_name` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `game_tbl`
--

INSERT INTO `game_tbl` (`game_id`, `game_name`) VALUES
(1, 'game_A'),
(2, 'game_B'),
(3, 'game_C');

-- --------------------------------------------------------

--
-- Table structure for table `submit_tbl`
--

CREATE TABLE `submit_tbl` (
  `submit_id` int(255) NOT NULL,
  `submit_time` datetime NOT NULL,
  `uid` int(255) NOT NULL,
  `game_id` int(255) NOT NULL,
  `level` int(11) NOT NULL,
  `score` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `submit_tbl`
--

INSERT INTO `submit_tbl` (`submit_id`, `submit_time`, `uid`, `game_id`, `level`, `score`) VALUES
(1, '2019-11-07 14:55:42', 4, 1, 1, 90),
(2, '2019-11-20 00:00:00', 5, 2, 2, 65),
(3, '2019-11-20 00:00:02', 5, 2, 2, 67),
(4, '2019-11-20 00:00:00', 5, 2, 2, 70),
(5, '2019-12-20 00:00:00', 6, 1, 1, 75),
(6, '2019-11-20 00:00:00', 5, 2, 1, 89),
(7, '2019-11-20 00:00:00', 6, 2, 2, 90),
(8, '2019-11-20 00:00:00', 6, 2, 3, 69),
(9, '2019-11-20 00:00:00', 8, 1, 1, 65),
(10, '2019-11-20 00:00:00', 5, 3, 1, 44),
(11, '2019-11-20 00:00:00', 5, 3, 1, 65),
(12, '2019-11-20 00:00:00', 5, 3, 2, 64),
(13, '2019-11-20 00:00:00', 5, 3, 3, 98),
(14, '2019-11-20 00:00:00', 7, 3, 3, 78),
(15, '2019-11-20 00:00:00', 7, 1, 2, 45),
(16, '2019-11-20 00:00:00', 9, 1, 1, 55);

-- --------------------------------------------------------

--
-- Table structure for table `user_tbl`
--

CREATE TABLE `user_tbl` (
  `uid` int(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_tbl`
--

INSERT INTO `user_tbl` (`uid`, `email`, `password`, `username`) VALUES
(4, 'a@gmail.com', '12345', 'a'),
(5, 'b@gmail.com', '12345', 'b'),
(6, 'c@gmail.com', '12345', 'c'),
(7, 'd@gmail.com', '12345', 'd'),
(8, 'e@gmail.com', '12345', 'e'),
(9, 'f@gmail.com', '12345', 'f'),
(10, 'g@gmail.com', '12345', 'g'),
(11, 'h@gmail.com', '12345', 'h'),
(12, 'i@gmail.com', '12345', 'i'),
(13, 'j@gmail.com', '12345', 'j'),
(14, 'k@gmail.com', '12345', 'k'),
(19, 'admin', 'admin', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_tbl`
--
ALTER TABLE `account_tbl`
  ADD PRIMARY KEY (`account_id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `game_id` (`game_id`);

--
-- Indexes for table `game_tbl`
--
ALTER TABLE `game_tbl`
  ADD PRIMARY KEY (`game_id`);

--
-- Indexes for table `submit_tbl`
--
ALTER TABLE `submit_tbl`
  ADD PRIMARY KEY (`submit_id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `game_id` (`game_id`);

--
-- Indexes for table `user_tbl`
--
ALTER TABLE `user_tbl`
  ADD PRIMARY KEY (`uid`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_tbl`
--
ALTER TABLE `account_tbl`
  MODIFY `account_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `game_tbl`
--
ALTER TABLE `game_tbl`
  MODIFY `game_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `submit_tbl`
--
ALTER TABLE `submit_tbl`
  MODIFY `submit_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `user_tbl`
--
ALTER TABLE `user_tbl`
  MODIFY `uid` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `account_tbl`
--
ALTER TABLE `account_tbl`
  ADD CONSTRAINT `account_tbl_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `user_tbl` (`uid`),
  ADD CONSTRAINT `account_tbl_ibfk_2` FOREIGN KEY (`game_id`) REFERENCES `game_tbl` (`game_id`);

--
-- Constraints for table `submit_tbl`
--
ALTER TABLE `submit_tbl`
  ADD CONSTRAINT `submit_tbl_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `user_tbl` (`uid`),
  ADD CONSTRAINT `submit_tbl_ibfk_2` FOREIGN KEY (`game_id`) REFERENCES `game_tbl` (`game_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
